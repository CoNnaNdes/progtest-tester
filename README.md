Usage example:

Image this is how you store your homeworks:

homeworks
```
.
| ptest
+ hw0
  | main.cpp
  + samples
    | 0000_in.txt
    | 0000_out.txt
    ...
    | XXXX_in.txt
    + XXXX_out.txt
```
to run tests + compile like progtest

**$** ./ptest hw0/samples hw0/main.cpp

**$** cd hw0

*(all commands below are executed inside hw0 directory)*

**$** ../ptest samples main.cpp

**$** g++ main.cpp && ../ptest

**$** g++ main.cpp && ../ptest samples a.out

**$** ../ptest

**$** ../ptest -l 

*(-l sets ptest to "lines" mode, in some cases more useful than output that "git diff" provides)*

***
*  If your folder is named ENG, Samples, samples or input_samples or contains sample files, it will be matched without explicitly providing the folder path. (naming your folder by one of the reserved names makes the script run faster).


*  Every folder must have sample inputs in the form of XXXX_in.txt, XXXX_out.txt, you can add your own, of course.


*  If no executable/source file is given to ptest the most recent file modified will be used.
***

You can find out more information @ "**$** ./ptest help"
